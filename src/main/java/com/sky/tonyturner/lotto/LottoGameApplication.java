package com.sky.tonyturner.lotto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@Profile("!test")
public class LottoGameApplication {
	private static final Logger logger = LoggerFactory.getLogger(LottoGameApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(LottoGameApplication.class, args);
	}
}

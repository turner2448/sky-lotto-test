package com.sky.tonyturner.lotto;

import com.sky.tonyturner.lotto.service.LottoDrawService;
import com.sky.tonyturner.lotto.service.LottoResults;
import com.sky.tonyturner.lotto.service.LottoResultsImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class LottoGameApplicationTests {

	@InjectMocks
	LottoResults lottoResults = new LottoResultsImpl();

	@Mock
	private LottoDrawService lottoDrawService;

	@Test
	/* Test case where player guesses all 6 numbers from example in Test brief */
	void testJackpot()  {
		Set<Integer> numbers = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6));
		when(lottoDrawService.draw()).thenReturn(new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6)));
		int results = lottoResults.checkNumbers(numbers);
		int winnings  = lottoResults.checkNumbers(numbers);

		assertTrue(winnings == 210000);
	}

	@Test
	/* Test case where player guesses 3 to 5 numbers correctly from example in Test brief */
	void testThreeToFiveNumbersCorrect()  {
		Set<Integer> numbers = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6));
		when(lottoDrawService.draw()).thenReturn(new HashSet<Integer>(Arrays.asList(3,4,5,6,7,8)));
		int results = lottoResults.checkNumbers(numbers);
		int winnings  = lottoResults.checkNumbers(numbers);

		assertTrue(winnings == 2056);
	}

	@Test
	/* Test case where player guesses < 3 numbers correctly from example in Test brief */
	void testFewerThanThreeNumbersCorrect()  {
		Set<Integer> numbers = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6));
		when(lottoDrawService.draw()).thenReturn(new HashSet<Integer>(Arrays.asList(7,8,9,10,11,12)));
		int results = lottoResults.checkNumbers(numbers);
		int winnings  = lottoResults.checkNumbers(numbers);

		assertTrue(winnings == 57);
	}

	/* TODO Test error scenarios */
}

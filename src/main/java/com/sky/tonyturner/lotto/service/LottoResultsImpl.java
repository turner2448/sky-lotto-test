package com.sky.tonyturner.lotto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LottoResultsImpl implements LottoResults {

    @Autowired
    LottoDrawService drawService;

    @Override
    public int checkNumbers(Set<Integer> numbers) {
        Set<Integer> draw = drawService.draw();

        /* Get intersection of two sets */
        Set<Integer> intersection = new HashSet<Integer>(draw);
        intersection.retainAll(numbers); //correct numbers

        /* remainder of drawn numbers after winning numbers removed*/
        Set<Integer> relativeCompliment = new HashSet<Integer>(draw);
        relativeCompliment.removeAll(intersection);

        /*System.out.println("numbers: " + numbers);
        System.out.println("draw: " + draw);
        System.out.println("intersection: " + intersection);
        System.out.println("relativeCompliment: " + relativeCompliment);*/

        int winnings = 0;

        switch(intersection.size()) {
            case 3: case 4: case 5:
                /*  £500 for every number they guessed, plus the multiplication of every number which they missed */
                Long guessedNumberSum = intersection.stream().mapToInt(Integer::intValue).count() * 500;
                Integer multiplication = relativeCompliment.stream().mapToInt(x->x).reduce(1, Math::multiplyExact);
                winnings = (int)(multiplication  + guessedNumberSum); //requirement to return int not long, to be on safe side??
                break;
            case 6:
                /* £10,000 * every number drawn */
                Integer jackpot = draw.stream().mapToInt(Integer::intValue).map(e -> e * 10000).sum();
                winnings = jackpot;
                break;
            default:
                /* sum of the numbers drawn */
                Integer sum = draw.stream().mapToInt(Integer::intValue).sum();
                winnings = sum.intValue();
                break;
        }

        return winnings;
    }
}

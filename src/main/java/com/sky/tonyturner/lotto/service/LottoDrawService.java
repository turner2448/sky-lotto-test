package com.sky.tonyturner.lotto.service;

import java.util.Set;

public interface LottoDrawService {
    public abstract Set<Integer> draw();
}

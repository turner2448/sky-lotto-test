package com.sky.tonyturner.lotto.service;

import java.util.Set;

public interface LottoResults {
    public int checkNumbers(Set<Integer> numbers);
}

package com.sky.tonyturner.lotto.bean;

import com.sky.tonyturner.lotto.service.LottoResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CommandLineTaskExecutor implements CommandLineRunner {

    @Autowired
    private LottoResults lottoResults;

    @Override
    public void run(String... args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your numbers");
        String numbers = in.next();
        String[] numberSplit = numbers.split(",");
        Set<Integer> customerNumbers = new HashSet<>(Arrays.stream(numberSplit).map(s -> Integer.parseInt(s)).filter(i -> 1 <= i && i <= 60).collect(Collectors.toSet()));
        if (customerNumbers.size() != 6) {
            System.out.println("Invalid Input. Please enter 6 unique comma separated numbers between 1 and 60");
            return;
        }
        System.out.println(lottoResults.checkNumbers(customerNumbers));
    }
}

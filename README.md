Job Spec: Job Details - Sky Careers

Take home exercise

Introduction

In Core Engineering we aim for all engineers to be cross-functional, to automate everything, and to have software managing infrastructure wherever practically possible. As such, we ask all candidates to independently complete and return a coding exercise. We understand that applicants come from a wide range of backgrounds and want to give you an opportunity to demonstrate your strengths throughout the recruitment process, while assessing skills key to the role. As part of the "take home" exercise, we are looking for simplicity, testing, and best practices.

You should aim to spend about an hour working on this exercise, but are welcome to take as much or little time as you think is necessary.

Exercise: Single Lottery Draw

An exciting new lottery game is being launched (another one!). The game is simple to play! Players guess six numbers ranging from 1 to 60 (inclusive).

Because 2020 has been such a difficult year for many, the lottery company has decided that they are feeling generous and that every ticket is a winner! Winnings are calculated as follows:

If the player guessed fewer than three correct numbers for the draw, they will win the sum of the numbers drawn. Example: player numbers: 1, 2, 3, 4, 5, 6 winning numbers: 7, 8, 9, 10, 11, 12 prize: £57
If the player guessed three to five numbers correctly, they will win £500 for every number they guessed, plus the multiplication of every number which they missed. Example: player numbers: 1, 2, 3, 4, 5, 6 winning numbers: 3, 4, 5, 6, 7, 8 prize: £2,056
If the player guessed all six numbers correctly, they will win £10,000 multiplied by the sum of every number drawn! Example: player numbers: 1, 2, 3, 4, 5, 6 winning numbers: 1, 2, 3, 4, 5, 6 prize: £210,000
You are entrusted with writing the software to calculate the winnings for the draw! The software can be written in any language and must be able to run from the command line. The input for your software will be a set of six numbers (the player's chosen numbers) and return an integer (being the prize value).

Interface:

Input: lottoResults <CSV of player numbers>
e.g. lottoResults 1,2,3,4,5,6

Output: Player winnings
e.g. 100

Notes:

You may use any language you wish, but the program or script must be runnable from a Unix based OS (or compatible) command line and must follow the above interface.
Include instructions on how to run the program or script and details of any dependencies.
List any assumptions which you have made.
Provide a zip file with everything required to run the program/zip with your name, e.g. John.Doe.zip.
Do not publish your solution in the public domain e.g. GitHub, a blog or Dropbox.

This isn't just a lotto, this is an M&S lotto.

To run from command line (Maven):
1. From current directory, extract zip file
2. cd lotto
3. ./mvnw spring-boot:run
4. Enter 6 comma separated numbers between 1 and 60 when prompted.
5. Your winnings will be returned to you as a number (no currency symbol).

Use ./mvnw test to run unit tests.

Java 11+ recommended.

[If you want to distribute the jar file, run ./mvnw package, cd target, java -jar lotto-0.0.1-SNAPSHOT.jar]

<h1>Assumptions</h1>
<p>The draw is entirely random (it wasn't entirely clear to me whether you wanted hard coded results, plus 
I wasn't sure what the lottoResults aspect as part of the input was meant to be).</p>

I was asked to anonymise just prior to submission, but had already built my name into the package structure (sorry).

<h1>Enhancements</h1>
<p>Add integration tests to compliment the unit tests - there is nothing testing the Spring context or other units
beyond the workhorse, LottoResults.</p>

More thorough unit testing, including error scenarios.



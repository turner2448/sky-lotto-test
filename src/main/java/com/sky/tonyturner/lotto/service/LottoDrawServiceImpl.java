package com.sky.tonyturner.lotto.service;

import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LottoDrawServiceImpl implements LottoDrawService {
    @Override
    public Set<Integer> draw() {
        final int MIN = 1;
        final int MAX = 60;
        Set<Integer> draw = new HashSet<Integer>();
        while(draw.size() < 6 ) {
            int random_int = (int)Math.floor(Math.random()*(MAX-MIN+1)+MIN);
            draw.add(random_int);
        }
        return draw;
    }
}
